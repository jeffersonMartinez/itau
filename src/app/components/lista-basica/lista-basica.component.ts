import { Component, OnInit, Input } from '@angular/core';
import { Todo } from 'src/app/model/todo';
import { TodosService } from 'src/app/services/todos.service';

@Component({
  selector: 'app-lista-basica',
  templateUrl: './lista-basica.component.html',
  styleUrls: ['./lista-basica.component.css']
})
export class ListaBasicaComponent implements OnInit {
  @Input() tipo: string = '';

  todoArray: Array<Todo> = [];

  busqueda: string = '';

  constructor(private todosService: TodosService) { }

  ngOnInit() {
    if (this.tipo === 'todos') {
      this.getTodo();
    } else {
      this.getItems();
    }

  }

  getTodo() {
    this.todosService.getTodo()
      .subscribe(data => {
        this.todoArray = data;
        console.log(data);
      }, err => console.error(err));
  }

  getItems() {
    this.todosService.consultarItems()
      .subscribe(data => {
        this.todoArray = data;
        console.log(data);
      }, err => console.error(err));
  }

}
