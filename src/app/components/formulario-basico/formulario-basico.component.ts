import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControlName, FormControl } from '@angular/forms';

@Component({
  selector: 'app-formulario-basico',
  templateUrl: './formulario-basico.component.html',
  styleUrls: ['./formulario-basico.component.css']
})
export class FormularioBasicoComponent implements OnInit {

  @Input() tipo: string;
  @Output() salida = new EventEmitter<any>();


  // crearForm
  crearForm: FormGroup;

  // Alias
  get getId() { return this.crearForm.get('id'); }
  get getTitle() { return this.crearForm.get('title'); }

  titulo: string = '';
  boton: string = '';

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.configuraCrearForm();
    this.configurarTemplate();
  }

  /**
   * Configura los atributos que cambian según el tipo de función a realizar
   */
  configurarTemplate() {
    switch (this.tipo) {
      case 'crear':
        this.titulo = 'Crear un item nuevo';
        this.boton = 'Guardar';
        break;
      case 'editar':
        this.titulo = 'Editar el item con el id ingresado';
        this.boton = 'Editar';
        break;
      case 'eliminar':
        this.titulo = 'Eliminar el item con el id ingresado';
        this.boton = 'Eliminar';
        break;
    }
  }


  /**
   * Configura el formulario reactivo
   */
  configuraCrearForm() {
    if (this.tipo !== 'eliminar') {
      this.crearForm = this.fb.group({
        id: ['', Validators.required],
        title: ['', Validators.required]
      });

    } else {
      this.crearForm = this.fb.group({
        id: ['', Validators.required]
      });
    }
  }

  /**
   * Pasa los parámetros del formulario al componente padre
   */
  validar() {
    const id = this.getId.value;
    const title = this.getTitle.value;

    this.salida.emit({ id, title });
  }


}
