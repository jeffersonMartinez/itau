import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
// import { ScrollingModule } from '@angular/cdk/scrolling';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Pipes
import { FilterPipe } from './common/pipes/filter.pipe';

// Pages
import { NavbarComponent } from './components/navbar/navbar.component';
import { CrearItemComponent } from './pages/crear-item/crear-item.component';
import { TodosComponent } from './pages/todos/todos.component';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

// Angular Material
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';

// Extras
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { FormularioBasicoComponent } from './components/formulario-basico/formulario-basico.component';
import { EliminarItemComponent } from './pages/eliminar-item/eliminar-item.component';
import { ActualizarItemComponent } from './pages/actualizar-item/actualizar-item.component';
import { ConsultarItemComponent } from './pages/consultar-item/consultar-item.component';
import { ListaBasicaComponent } from './components/lista-basica/lista-basica.component';



@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    FilterPipe,
    NavbarComponent,
    CrearItemComponent,
    FormularioBasicoComponent,
    EliminarItemComponent,
    ActualizarItemComponent,
    ConsultarItemComponent,
    ListaBasicaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // ScrollingModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
