import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../model/todo';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TodosService extends BaseService {

  item: string = 'item';

  constructor(
    private http: HttpClient,
    private db: AngularFirestore
  ) {
    super();
  }

  /**
   * Consume el endpoint para consultar todos
   */
  getTodo() {
    return this.http.get<Array<Todo>>(
      this.urlTodos,
      { headers: this.generateBasicHeaders() }
    );
  }

  /**
   * Guarda un item en firebase
   * @param id 
   * @param title 
   */
  crearItem(id: number, title: string) {
    return this.db.collection(this.item)
      .doc(id.toString())
      .set({
        id,
        title
      });
  }

  /**
   * Actualiza un item en firebase, según el id
   * @param id 
   * @param title 
   */
  actualizarItem(id: number, title: string) {
    return this.db.collection(this.item)
      .doc(id.toString()).update({ id, title });
  }

  /**
   * Elimina el item con el id enviado
   * @param id
   */
  eliminarItem(id: number) {
    return this.db.collection(this.item)
      .doc(id.toString()).delete();
  }

  /**
   * Consulta todos los items almacenados en firebase
   */
  consultarItems() {
    return this.db.collection<Todo>(this.item).valueChanges();
  }

}
