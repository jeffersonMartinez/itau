import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

// @Injectable({
//   providedIn: 'root'
// })
export class BaseService {
  urlTodos: string = 'https://jsonplaceholder.typicode.com/todos';

  constructor() { }

  protected generateBasicHeaders(): HttpHeaders {
    return new HttpHeaders({
        'Content-Type': 'application/json'
    });
}
}
