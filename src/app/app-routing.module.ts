import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodosComponent } from './pages/todos/todos.component';
import { CrearItemComponent } from './pages/crear-item/crear-item.component';
import { ActualizarItemComponent } from './pages/actualizar-item/actualizar-item.component';
import { ConsultarItemComponent } from './pages/consultar-item/consultar-item.component';
import { EliminarItemComponent } from './pages/eliminar-item/eliminar-item.component';

const routes: Routes = [
  { path: 'todos', component: TodosComponent },
  { path: 'crear-item', component: CrearItemComponent },
  { path: 'editar-item', component: ActualizarItemComponent },
  { path: 'eliminar-item', component: EliminarItemComponent },
  { path: 'consultar-item', component: ConsultarItemComponent },
  { path: '', redirectTo: 'todos', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
