import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarItemComponent } from './consultar-item.component';

describe('ConsultarItemComponent', () => {
  let component: ConsultarItemComponent;
  let fixture: ComponentFixture<ConsultarItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
