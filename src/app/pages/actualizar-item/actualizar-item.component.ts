import { Component, OnInit } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-actualizar-item',
  templateUrl: './actualizar-item.component.html',
  styleUrls: ['./actualizar-item.component.css']
})
export class ActualizarItemComponent implements OnInit {

  constructor(
    private todosService: TodosService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  /**
   * Obtiene el id y el title para enviarlo a firebase
   */
  guardar(event: any) {
    const id = event.id;
    const title = event.title;

    this.spinner.show();

    this.todosService.actualizarItem(id, title)
      .then(res => {
        console.log(res);
        this.spinner.hide();
        this.toastr.success('La edición se realizó exitosamente');
      })
      .catch(err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Proceso fallido, por favor intenta nuevamente');
      });
  }

}
