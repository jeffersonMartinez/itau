import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarItemComponent } from './actualizar-item.component';

describe('ActualizarItemComponent', () => {
  let component: ActualizarItemComponent;
  let fixture: ComponentFixture<ActualizarItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
