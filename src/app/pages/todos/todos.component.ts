import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/model/todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todoArray: Array<Todo> = [];

  busqueda: string = '';

  constructor() {

  }

  ngOnInit() {
  }

}
