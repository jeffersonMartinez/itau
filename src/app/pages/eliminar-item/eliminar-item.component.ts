import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TodosService } from 'src/app/services/todos.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-eliminar-item',
  templateUrl: './eliminar-item.component.html',
  styleUrls: ['./eliminar-item.component.css']
})
export class EliminarItemComponent implements OnInit {

  constructor(
    private todosService: TodosService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  /**
   * Obtiene el id para enviarlo a firebase
   */
  eliminar(event: any) {
    const id = event.id;

    this.spinner.show();

    this.todosService.eliminarItem(id)
      .then(res => {
        console.log(res);
        this.spinner.hide();
        this.toastr.success('La eliminación se realizó exitosamente');
      })
      .catch(err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Proceso fallido, por favor intenta nuevamente');
      });
  }

}
