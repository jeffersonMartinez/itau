import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TodosService } from '../../services/todos.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-crear-item',
  templateUrl: './crear-item.component.html',
  styleUrls: ['./crear-item.component.css']
})
export class CrearItemComponent implements OnInit {

  constructor(
    private todosService: TodosService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  /**
   * Obtiene el id y el title para enviarlo a firebase
   */
  guardar(event: any) {
    const id = event.id;
    const title = event.title;

    this.spinner.show();

    this.todosService.crearItem(id, title)
      .then(res => {
        console.log(res);
        this.spinner.hide();
        this.toastr.success('La creación se realizó exitosamente');
      })
      .catch(err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Proceso fallido, por favor intenta nuevamente');
      });
  }

}
