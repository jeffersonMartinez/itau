// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDA6PN9YBDhoeFmtMRs8Cd-t4SuQCijWmo',
    authDomain: 'itau-2f4c0.firebaseapp.com',
    databaseURL: 'https://itau-2f4c0.firebaseio.com',
    projectId: 'itau-2f4c0',
    storageBucket: '',
    messagingSenderId: '161940610069',
    appId: '1:161940610069:web:9bea8114ba792ced'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
